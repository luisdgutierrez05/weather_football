class Weather
  include DataProcessor

  # Instance Methods
  def smallest_temperature_spread
    calculate_smallest(:mnt, :mxt, :dy)
    show_result("The day with the smallest temperature spread was")
  end
end

# ------------------------------
# How Run it.
# ------------------------------
# require './data_processor.rb'
# require './weather.rb'

# w = Weather.new
# file_path = './weather.dat'
# w.read_data(file_path)
# w.smallest_temperature_spread
