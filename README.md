# Team International Test
## Weather/Football Code Exercise
#
> Delivery Date: Wednesday at 11:00 am, March 29th
> Developed By: Luis D. Gutiérrez
#
## Part One: Weather Data
In weather.dat you’ll find daily weather data for Morristown, NJ for June 2002. Download this text file, then write a program to output the day number (column one) with the smallest temperature spread (the maximum temperature is the second column, the minimum the third column).

### Part Two: Soccer League Table
The file football.dat contains the results from the English Premier League for 2001/2. The columns labeled ‘F’ and ‘A’ contain the total number of goals scored for and against each team in that season (so Arsenal scored 79 goals against opponents, and had 36 goals scored against them). Write a program to print the name of the team with the smallest difference in ‘for’ and ‘against’ goals.

### Part Three: DRY Fusion
Take the two programs written previously and factor out as much common code as possible, leaving you with two smaller. programs and some kind of shared functionality.

#### How run it to get the number day with the smallest temperature spread:
```sh
$ cd weather_football
$ irb
$ 2.1.5 :001 > require './data_processor.rb'
$ 2.1.5 :002 > require './weather.rb'
$ 2.1.5 :003 > w.read_data(file_path)
$ 2.1.5 :004 > w.smallest_temperature_spread
```

#### How run it to get the team with the smallest difference goals:
```sh
$ cd weather_football
$ irb
$ 2.1.5 :001 > require './data_processor.rb'
$ 2.1.5 :002 > require './football.rb'
$ 2.1.5 :003 > f.read_data(file_path)
$ 2.1.5 :004 > f.smallest_difference_goals
```
