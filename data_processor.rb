module DataProcessor
  # Attributes
  attr_accessor :headers, :data, :smallest_value, :result

  # Instance Methods

  # Public: Read file
  #
  # file_path - path of the file. (String)
  #
  # Examples
  #
  #   add('./weather.dat')
  #   # => [" A B C\r\n", "\r\n", " 1 10 5\r\n"]
  #
  # Get the headers and an array with data readed from file.
  def read_data(file_path)
    @data = File.readlines(file_path).map(&:split)
    @headers = @data[0].map{ |v| v.downcase.to_sym }
    @data.delete_at(0)
  end

  # Public: Calculate the smallest value
  #
  # min_column    - Name of the column with minimum value.     (Symbol)
  # max_column    - Name of the column with maximum value.     (Symbol)
  # output_column - Name of the column with reference to show. (Symbol)
  #
  # Examples
  #
  #   calculate_smallest(:mnt, :mxt, :dy)
  #   # => 10
  #
  # Calculates the smallest value and result from readed data.
  def calculate_smallest(min_column, max_column, output_column)
    min_index    = @headers.find_index(min_column)
    max_index    = @headers.find_index(max_column)
    output_index = @headers.find_index(output_column)

    data_results = build_results(min_index, max_index, output_index)
    result = data_results.min{ |a,b| a[:difference] <=> b[:difference] }

    @smallest_value = result[:difference]
    @result = result[:name]
  end

  def show_result(message)
    puts "-" * 60
    puts "* #{message} #{@result} *"
    puts "-" * 60
  end

  private

  # Private: Check if object is Float type
  #
  # object - Object to validate
  #
  # Returns true when the object is a Float value
  #         or false when the object is not a Float value.
  def is_float?(object)
    true if Float(object) rescue false
  end

  def build_results(min_index, max_index, output_index)
    results = []
    @data.each do |line|
      if is_float?(line[min_index]) && is_float?(line[max_index])
        difference = (line[max_index].to_f - line[min_index].to_f).abs
        results << { name: line[output_index], difference: difference }
      end
    end
    results
  end
end
