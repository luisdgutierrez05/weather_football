class Football
  include DataProcessor

  # Instance Methods
  def smallest_difference_goals
    calculate_smallest(:f, :a, :team)
    show_result("The team with the smallest difference goals was")
  end
end

# ------------------------------
# How Run it.
# ------------------------------
# require './data_processor.rb'
# require './football.rb'

# f = Football.new
# file_path = './football.dat'
# f.read_data(file_path)
# f.smallest_difference_goals
